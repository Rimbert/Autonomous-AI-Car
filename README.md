# Autonomous Car Upgrading
Il s'agit du repository contenant le code pour faire marche une petite voiture en autonomie (GPIO). 
La caméra de la voiture reconnaît un circuit prédéfini et s'arrête lorsqu'elle rencontre un panneau stop ou un feu rouge.
Un modèle de deep learning en keras est utilisé pour effectuer ces actions.
