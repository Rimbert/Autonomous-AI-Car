import RPi.GPIO as gpio
import time

def init_gpio():
    gpio.setmode(gpio.BCM)
    gpio.setup(17, gpio.OUT)
    gpio.setup(22, gpio.OUT)
    gpio.setup(23, gpio.OUT)
    gpio.setup(24, gpio.OUT)
def forward():
    #init_gpio()
    gpio.output(17, False)
    gpio.output(22, True)
    gpio.output(23, False)
    gpio.output(24, True)
    #gpio.cleanup()
def backward():
    #init_gpio()
    gpio.output(17, True)
    gpio.output(22, False)
    gpio.output(23, True)
    gpio.output(24, False)
    #gpio.cleanup()
def left():
    #init_gpio()
    gpio.output(17, False)
    gpio.output(22, True)
    gpio.output(23, True)
    gpio.output(24, False)
    #gpio.cleanup()
def right():
    #init_gpio()
    gpio.output(17, True)
    gpio.output(22, False)
    gpio.output(23, False)
    gpio.output(24, True)
    #gpio.cleanup()
def stop():
    #init_gpio()
    gpio.output(17, False)
    gpio.output(22, False)
    gpio.output(23, False)
    gpio.output(24, False)
    #gpio.cleanup()
def clean_and_stop():
    init_gpio()
    gpio.output(17, False)
    gpio.output(22, False)
    gpio.output(23, False)
    gpio.output(24, False)
    gpio.cleanup()
def clean():
    gpio.cleanup()
